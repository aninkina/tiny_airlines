package com.example.viewmodel;

public class MyState {
    public int scoreTeamA = 0;
    public int scoreTeamB = 0;

    @Override
    public String toString() {
        return "A: " + scoreTeamA + ";  B: " + scoreTeamB;
    }
}
