package com.example.viewmodel;

import android.view.View;
import android.widget.CheckBox;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class MyViewHolder extends RecyclerView.ViewHolder {

    TextView title;
    RatingBar ratingBar;
    CheckBox checkBox;

    public MyViewHolder(@NonNull View itemView) {
        super(itemView);
        title = itemView.findViewById(R.id.name);
        ratingBar = itemView.findViewById(R.id.rating_bar);
        checkBox = itemView.findViewById(R.id.check_box);
    }
}
