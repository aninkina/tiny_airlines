package com.example.viewmodel;

public class RecyclerState {
    public String title;
    public String checkBoxText;
    public int id;

    public RecyclerState(String title, String checkBoxText, int id){
        this.title = title;
        this.checkBoxText = checkBoxText;
        this.id = id;
    }
}
