package com.example.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;


public class ScoreViewModel extends ViewModel {

    private final MutableLiveData<MyState> stateLiveData = new MutableLiveData<>();

    public LiveData<MyState> getState() {
        return stateLiveData;
    }

    public void setStateLiveData() {
        stateLiveData.setValue(new MyState());
    }
}
