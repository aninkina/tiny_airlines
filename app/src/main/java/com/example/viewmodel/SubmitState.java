package com.example.viewmodel;

public class SubmitState {
    public int id;
    public float rating;
    public boolean skipped = true;

    public SubmitState(int id){
        this.id = id;
    }


}
