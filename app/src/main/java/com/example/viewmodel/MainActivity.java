package com.example.viewmodel;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;

    private ScoreViewModel mViewModel;

    private MyAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initRecyclerView();
        setSubmitButtonListener();

        mViewModel = ViewModelProviders.of(this).get(ScoreViewModel.class);

        mViewModel.getState().observe(this, new Observer<MyState>() {
            @Override
            public void onChanged(@Nullable final MyState state) {
                if (state == null) {
                    return;
                }
                // Update your views here
                displayForTeamA(state.scoreTeamA);
                displayForTeamB(state.scoreTeamB);
            }
        });


        if (mViewModel.getState().getValue() == null) {
            mViewModel.setStateLiveData();
        }
        displayForTeamA(mViewModel.getState().getValue().scoreTeamA);
        displayForTeamB(mViewModel.getState().getValue().scoreTeamB);
    }

    private void displayForTeamB(int scoreTeamB) {
        //textB.setText(Integer.toString(scoreTeamB));
    }

    private void displayForTeamA(int scoreTeamA) {
        // textA.setText(Integer.toString(scoreTeamA));
    }


    private void initRecyclerView() {
        recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this) {
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        });

        List<RecyclerState> stateList = new ArrayList<>();
        stateList.add(new RecyclerState("Title1", "there were this thing1", 0));
        stateList.add(new RecyclerState("Title2", "there were this thing2", 1));
        stateList.add(new RecyclerState("Title4", null, 2));
        stateList.add(new RecyclerState("Title3", "there were this thing3", 3));
        stateList.add(new RecyclerState("Title5", null, 4));
        stateList.add(new RecyclerState("Title6", "there no food", 5));


        adapter = new MyAdapter(stateList);
        recyclerView.setAdapter(adapter);
    }

    private void setSubmitButtonListener() {

        findViewById(R.id.b_submit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                EditText editTextComment = findViewById(R.id.et_feedback);
                String comment = editTextComment.getText().toString();


                StringBuilder stringBuilder = new StringBuilder();
                List<SubmitState> submitStates = adapter.getSubmitStates();
                for (SubmitState el : submitStates) {
                    stringBuilder.append(el.id).append(' ').append(el.rating).append(' ').append(el.skipped).append('\n');
                }
                stringBuilder.append(comment);

                System.out.println(stringBuilder.toString());
            }
        });

    }
}