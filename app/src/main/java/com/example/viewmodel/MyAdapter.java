package com.example.viewmodel;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class MyAdapter extends RecyclerView.Adapter<MyViewHolder> {

    private List<RecyclerState> states;

    private List< SubmitState> submitStates = new ArrayList<>();

    public MyAdapter(List<RecyclerState> states) {
        super();
        this.states = states;

        for (RecyclerState el : states) {
            submitStates.add(new SubmitState(el.id));
        }
    }

    public List<SubmitState> getSubmitStates() {
        return submitStates;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recyclerview_layout, parent, false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {
        final RecyclerState state = states.get(position);
        holder.checkBox.setVisibility(state.checkBoxText == null ? View.GONE : View.VISIBLE);
        holder.checkBox.setText(state.checkBoxText);
        holder.title.setText(state.title);

        holder.checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.ratingBar.setRating(0);
                holder.ratingBar.setEnabled(!holder.checkBox.isChecked());
                submitStates.get(position).skipped = holder.checkBox.isChecked();
            }
        });

        holder.ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            public void onRatingChanged(RatingBar ratingBar, float rating,
                                        boolean fromUser) {

                submitStates.get(position).skipped = false;
                submitStates.get(position).rating = rating;
            }
        });
    }


    @Override
    public int getItemCount() {
        return states.size();
    }
}
